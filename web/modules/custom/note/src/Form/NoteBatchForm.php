<?php

namespace Drupal\note\Form;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Note settings for this site.
 */
class NoteBatchForm extends FormBase {

  /**
   * An array with available node types.
   *
   * @var array|mixed
   */
  protected $nodeBundles;

  /**
   * Batch Builder.
   *
   * @var \Drupal\Core\Batch\BatchBuilder
   */
  protected $batchBuilder;

  /**
   * Node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * BatchForm constructor.
   */
  public function __construct(EntityTypeBundleInfo $entity_type_bundle_info, NodeStorageInterface $node_storage) {
    $this->nodeBundles = $entity_type_bundle_info->getBundleInfo('node');
    array_walk($this->nodeBundles, function (&$a) {
      $a = $a['label'];
    });
    $this->nodeStorage = $node_storage;
    $this->batchBuilder = new BatchBuilder();
  }

  /**
   * We get a list of all available content types for the entity.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.bundle.info'),
      $container->get('entity_type.manager')->getStorage('node')
    );
  }

  /**
   * Determines the ID of a form.
   */
  public function getFormId() {
    return 'note_batch';
  }

  /**
   * Builds and processes a form for a given form ID.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Date for update'),
      '#required' => TRUE,
      '#default_value' => new DrupalDateTime('2000-01-01 00:00:00'),
      '#description' => $this->t('Enter the date regarding which the status of the nodes will change.'),
    ];

    $form['checkbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reset Status'),
      '#default_value' => FALSE,
      '#description' => $this->t('If set, then the Status field is reset to N/A.'),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['run'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * Retrieves, populates, and processes a form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $nodes = $this->getNodes('note');
    $reset = $form_state->getValue('checkbox');
    $this->batchBuilder->setFile(drupal_get_path('module', 'note') . '/src/Form/NoteBatchForm.php');
    if ($reset == FALSE) {
      $operation = $form_state->getValue('date');
    }
    else {
      $operation = FALSE;
    }
    $this->batchBuilder->addOperation([$this, 'processItems'], [$nodes, $operation]);
    $this->batchBuilder->setFinishCallback([$this, 'finished']);
    batch_set($this->batchBuilder->toArray());
  }

  /**
   * Processor for batch operations.
   */
  public function processItems($items, $operation, array &$context) {
    $limit = 50;
    if (empty($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($items);
    }
    if (empty($context['sandbox']['items'])) {
      $context['sandbox']['items'] = $items;
    }
    $counter = 0;
    if (!empty($context['sandbox']['items'])) {
      if ($context['sandbox']['progress'] != 0) {
        array_splice($context['sandbox']['items'], 0, $limit);
      }
      foreach ($context['sandbox']['items'] as $item) {
        if ($counter != $limit) {
          $this->processItem($item, $operation);
          $counter++;
          $context['sandbox']['progress']++;
          $context['message'] = $this->t('Now processing node :progress of :count', [
            ':progress' => $context['sandbox']['progress'],
            ':count' => $context['sandbox']['max'],
          ]);
          $context['results']['processed'] = $context['sandbox']['progress'];
        }
      }
    }
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Process single item.
   */
  public function processItem($nid, $operation) {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->nodeStorage->load($nid);
    if ($operation == FALSE) {
      $node->set('field_list_status', NULL)->save();
    }
    else {
      if ($node->getCreatedTime() >= $operation->getTimestamp()) {
        $node->set('field_list_status', 'Actual')->save();
      }
      else {
        $node->set('field_list_status', 'Expired')->save();
      }
    }
  }

  /**
   * Finished callback for batch.
   */
  public function finished($success, $results, $operations) {
    $message = $this->t('Status changed for @count entries', [
      '@count' => $results['processed'],
    ]);
    $this->messenger()
      ->addStatus($message);
  }

  /**
   * Load all nids for specific type.
   */
  public function getNodes($type) {
    return $this->nodeStorage->getQuery()
      ->condition('type', $type)
      ->execute();
  }

}
