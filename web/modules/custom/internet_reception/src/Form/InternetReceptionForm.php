<?php

namespace Drupal\internet_reception\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Contains \Drupal\internet_reception\Form\InternetReceptionForm.
 */
class InternetReceptionForm extends FormBase {

  /**
   * Create Fields.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => 'Name',
      '#maxlength' => '30',
      '#required' => TRUE,
    ];

    $form['email'] = [
      '#type' => 'email',
      '#title' => 'Email',
      '#required' => TRUE,
    ];

    $form['age'] = [
      '#type' => 'number',
      '#title' => 'Age',
      '#min' => '1',
      '#max' => '100',
      '#step' => '1',
      '#required' => TRUE,
    ];

    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => 'Subject',
      '#maxlength' => '200',
      '#size' => '150',
      '#required' => TRUE,
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => 'Message',
      '#maxlength' => '1200',
      '#size' => '1200',
      '#required' => TRUE,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * Return results.
   */
  public function getFormId() {
    return 'internet_reception_form';
  }

  /**
   * Validation check.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $title = $form_state->getValue('name');
    $is_text = preg_match("/[^(\w)|(\x7F-\xFF)|(\s)]/", $title, $match);
    $is_number = preg_match("/^\d+$/", $title, $match);

    if ($is_text > 0 || $is_number > 0) {
      $form_state->setErrorByName('name', $this->t('The name is entered incorrectly.'));
    }
  }

  /**
   * Form submission.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $to = 'test@test.com';
    $subject = 'Internet Reception';

    $message = $this->t('
      Name: @name
      Email: @email
      Age: @age
      Subject: @subject
      Message: @message',
      [
        '@name' => $form_state->getValue('name'),
        '@email' => $form_state->getValue('email'),
        '@age' => $form_state->getValue('age'),
        '@subject' => $form_state->getValue('subject'),
        '@message' => $form_state->getValue('message'),
      ]
    );
    mail($to, $subject, $message);
    $this->messenger()->addStatus($this->t('A message was sent to the administrator by mail!'));
  }

}
