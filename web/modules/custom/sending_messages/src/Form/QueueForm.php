<?php

namespace Drupal\sending_messages\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * We declare a form for sending letters.
 */
class QueueForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'sending_messages.message';

  /**
   * Determines the ID of a form.
   */
  public function getFormId() {
    return 'sending_messages_admin_message';
  }

  /**
   * Gets the configuration names that will be editable.
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Builds and processes a form for a given form ID.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message to users'),
      '#default_value' => $this->t('Hello, [sending_messages:user]! Your email address: [sending_messages:mail]. Sent from: [site:mail].'),
      '#description' => $this->t(
        'Enter a message that will be sent to all registered users.<br>You can use tokens in the message ([sending_messages:mail] and [sending_messages:user]).'),
    ];

    $queue = \Drupal::queue('sending_messages');
    if ($number_of_items = $queue->numberOfItems()) {
      $form['info_text'] = [
        '#type' => 'markup',
        '#markup' => t('This queue is already running.<br>Queue: @number', [
          '@number' => $number_of_items,
        ]),
      ];

      $form['checkbox'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Delete Queue'),
        '#default_value' => FALSE,
        '#description' => $this->t('Deletes all messages added to the queue.'),
      ];
    }
    else {
      $form['info_text'] = [
        '#type' => 'markup',
        '#markup' => t('There are no messages in the queue.'),
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * Retrieves, populates, and processes a form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $queue = \Drupal::queue('sending_messages');
    $del = $form_state->getValue('checkbox');

    if ($del == TRUE) {
      $queue->deleteQueue();
    }
    else {
      if ($queue->numberOfItems() > 102) {
        \Drupal::messenger()->addMessage('Queue Overflow!');
      }
      else {
        $this->configFactory->getEditable(static::SETTINGS)
          ->set('message', $form_state->getValue('message'))
          ->save();
        parent::submitForm($form, $form_state);
        $query = \Drupal::database()->select('users_field_data', 'u')
          ->fields('u', ['uid', 'name', 'mail'])
          ->condition('u.status', 1);
        $result = $query->execute();
        $queue->createQueue();

        foreach ($result as $row) {
          $queue->createItem([
            'uid' => $row->uid,
            'name' => $row->name,
            'mail' => $row->mail,
          ]);
        }
      }
    }

  }

}
