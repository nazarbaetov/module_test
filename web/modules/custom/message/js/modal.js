(function ($) {
  Drupal.behaviors.messageBehavior = {
    attach: function (context, settings) {
      var width_js = drupalSettings.width;
      var height_js = drupalSettings.height;
      var color_js = drupalSettings.color;

      $(".messages__wrapper").css({
        width: width_js,
      });

      $(".message_window").css({
        height: height_js,
      });

      $(".title_js").css({
        background: color_js,
      });

      $("button, .cover").click(function () {
        $(".modal_window").hide();
      });

      let elem = document.querySelector('#elem');
      let offsetX;
      let offsetY;

      elem.addEventListener('dragstart', function (event) {
        offsetX = event.offsetX;
        offsetY = event.offsetY;
      });

      elem.addEventListener('dragend', function (event) {
        elem.style.top = (event.pageY - offsetY) + 'px';
        elem.style.left = (event.pageX - offsetX) + 'px';
      });
    }
  };
})(jQuery);
