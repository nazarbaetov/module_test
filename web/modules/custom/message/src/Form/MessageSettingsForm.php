<?php

namespace Drupal\message\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure message settings for this site.
 */
class MessageSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'message.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'message_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['title_window'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $config->get('title_window'),
      '#minlength' => '1',
      '#maxlength' => '50',
      '#size' => '55',
      '#placeholder' => 'Messages',
      '#description' => $this->t('Enter a window title'),
    ];

    $form['width_window'] = [
      '#type' => 'number',
      '#title' => $this->t('Width window'),
      '#default_value' => $config->get('width_window'),
      '#min' => '180',
      '#max' => '1200',
      '#step' => '1',
      '#placeholder' => '500',
      '#description' => $this->t('Enter the window width in pixels'),
    ];

    $form['height_window'] = [
      '#type' => 'number',
      '#title' => $this->t('Height window'),
      '#default_value' => $config->get('height_window'),
      '#min' => '80',
      '#max' => '550',
      '#step' => '1',
      '#placeholder' => '300',
      '#description' => $this->t('Enter the window width in pixels'),
    ];

    $form['color_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Color title'),
      '#default_value' => $config->get('color_title'),
      '#maxlength' => '7',
      '#size' => '10',
      '#placeholder' => '#0071b3',
      '#description' => $this->t('Enter the title color in the format #000000'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $title = $form_state->getValue('title_window');
    $is_text = preg_match("/[^(\w)|(\x7F-\xFF)|(\s)]/", $title, $match);
    $is_number = preg_match("/^\d+$/", $title, $match);
    if ($is_text > 0 || $is_number > 0) {
      $form_state->setErrorByName('title_window', $this->t('The title is entered incorrectly.'));
    }

    $color = $form_state->getValue('color_title');
    $is_num = preg_match('/^#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?$/i', $color, $match);
    if ($is_num == NULL) {
      $form_state->setErrorByName('color_title', $this->t('Enter the title color in the format #000000'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('title_window', $form_state->getValue('title_window'))
      ->set('width_window', $form_state->getValue('width_window'))
      ->set('height_window', $form_state->getValue('height_window'))
      ->set('color_title', $form_state->getValue('color_title'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
