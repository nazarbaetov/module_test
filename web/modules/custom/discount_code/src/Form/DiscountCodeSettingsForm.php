<?php

namespace Drupal\discount_code\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure discount_code settings for this site.
 */
class DiscountCodeSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'discount_code.message';

  /**
   * Determines the ID of a form.
   */
  public function getFormId() {
    return 'discount_code_admin_message';
  }

  /**
   * Gets the configuration names that will be editable.
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * Builds and processes a form for a given form ID.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message for new users'),
      '#default_value' => $config->get('message'),
      '#description' => $this->t('Use the following tokens:<br/>new user name: [discount_code:user]<br/>discount code: [discount_code:code]'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Retrieves, populates, and processes a form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('message', $form_state->getValue('message'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
