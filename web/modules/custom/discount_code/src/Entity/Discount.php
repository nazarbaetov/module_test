<?php

namespace Drupal\discount_code\Entity;

/**
 * Contains \Drupal\discount_code\Entity\Discount.
 *
 * @file
 */
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines the discount_code entity.
 *
 * @ingroup discount_code
 *
 * @ContentEntityType(
 *   id = "discount_code",
 *   label = @Translation("discount_code"),
 *   base_table = "discount_code",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 * )
 */
class Discount extends ContentEntityBase implements ContentEntityInterface {

  /**
   * Class for defining entity fields.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('ID'))
      ->setReadOnly(TRUE);

    $fields['user'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User ID'))
      ->setDescription(t('The ID of the associated user.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default');

    $fields['code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('String field'))
      ->setDescription(t('Code'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 10,
        'text_processing' => 0,
      ]
      );
    return $fields;
  }

}
